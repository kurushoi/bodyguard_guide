
# Introduction
I am writing this guide in the hopes of helping people to start in the path of **healing operations** as a Bodyguard Bounty Hunter. This **NOT a PvP guide, or even a generic PvE**, some of the considerations here only apply to a specific gear choice and build which is the one I find most useful for operations. I am a recent player to the game and I would not say I am particularly experienced, that said I am a bounty hunter/sorcerer healer for my guild's progression ops group.

I intend to organize this guide in a way that the most important and easy to practice information comes first and then as you get used with the fundamental stuff you can come back and incorporate the rest of the information.

# Healing Mindset

Put it simply: you have to keep people alive. 

An encounter during an operation is **not a linear event where a certain fixed amount of damage** is going to be dealt and you have to generate an amount of fixed healing. The fight ebbs and flows, there are tension moments and there are relief moments, sometimes there is heavy damage, sometimes there is almost no damage at all, sometimes **there are surprise damage spikes**. 

Your job is NOT to produce a gazillion hps throughout the fight, **your job is to detect where and when the heal is needed and then deliver the right amount of healing in the right place and time, preferably not compromising your abilities to respond to future events**.

If you think of the operation group as a music group, the tank is the drummer he is the one that defines the rhythm of the encounter, the healer is the bass player connecting the rhythm with the harmonic foundation of the music and the dps are the singers and guitar players, they are not as tied to the rhythm of the music, the melody has a life of its own provided it adheres to the harmonic foundation.

Just like the bass if you **sacrifice your rhythm to shred, things are going to fall apart**, it does not matter how well you are able to shred if it is not serving the music it will be a failure. The best compliment to a bass player is not how well you play, it is how good it feels to play with you.

This is all true for healers, if you do your part well, the operation is going to run smoothly, you will not be noticed, you should not be noticed, but people will feel good about it, your objective is to make people feel that when you are part of the group the game is going to be fun and fulfilling. 

Sometimes the music is going to ask shredding of you, you need to have the technical skills to do it, without letting the beat drop tho, so you have to have in your belt the ability, the **potential** to pull all those "big healz", but it needs to be done in a tasteful, deliberate way.

## Meters

Meters are a bless and a curse, they have the potential to draw out the best of people at the same time they have the potential to draw out the worse in people.

On one hand, they are a valuable tool for you to access the potential of different configuration of stats, the contribution of abilities to heal and damage, to give you feedback if you are performing or under-performing compared to other people or yourself across time.

On the other hand, people tend to get competitive and try to become the one that shreds the most, the one in the spotlight, instead of taking decisions that serve the success of the group people start to take decisions that produce the highest number. But how? You ask, how come producing the highest amount of damage goes against helping the group?

There are many ways that can happen, because the hps/ehps is only a partial view of the fight and does not give a full picture. Just to give some examples on how the need to appear in the parse can lead to bad decisions healing:

- Area spells tend to produce more raw healing than single target spells, but during a damage spike if you do not switch to single target to save another player, even if you are generating more healing number wise, that player death may make the difference between a successful clear or a wipe. Lets say a tank dies, you are going to produce sometimes a lot more healing because of that, the boss starts to eat away the group with cleaves, you get to put in a huge amount of area heals until the party wipes. If the other player is a healer, you get to make up for his heals until you get overwhelmed and the party wipes. Or maybe that player was a dps, and you think it is fine then, until the boss enrages for lack of dps, and guess what? You get to do big heals until the party wipes.

- You fail to do proper resource management in order to generate more heal. In order to preserve resources you need to sacrifice max heals, you need to do that to have the potential to do max heals when they are really needed, if you expend all the time your heat, your charges, your cooldowns, chances are that when they are really needed you wont have them. 

- You do not cater to the mechanics to heal only, during the fight there are other things to be done besides healing, cleansing for example, positioning yourself away from harm, etc. If you go into the hyper healing mode and start to see your global cooldown as too precious to waste on non healing stuff, you are going likely to fail.

Another way that the meter can be detrimental is you are doing everything right and you come up lower in the meter and get discouraged. It may lead you to think you are not good healer, not realizing that the others may be employing the previous tactics to come up on top.

You should always have you meter active, but you must understand that it is a tool to help to understand your potential but it is not the end goal or the whole truth.

## Do not slack
Somethings are absolutely not excusable, you should not be trying to be top hps, but that does not mean that you can allow yourself to have your gear in shambles and spend most of the fight sucking your thumb. You should try to gear yourself as well as it is possible, however the strategy you choose for stats, alacrity or crits, or whatever there is no excuse for not gearing up.

During the ops **you should always be doing something actively to contribute for the success of the operation**. Be it healing in its many forms, using active resource management abilities to generate resources or if the going is real easy, dpsing. There is no idle time, every second is an opportunity to do something that helps the group to overcome the fight.

## Mobility and Survival
During a swotor operation you are going to be **constantly in motion** and damage towards non tank toons is pretty common, it is almost a guarantee that you are going to move during fights to avoid damage and other negative effects and you and other non tank toons are going to take damage.

Because of that you have to invest in your survivability, you need to take the damage mitigation utilities and the following cannot be overstated: **abilities that allow you to heal while moving are precious.** Any gear or utility that makes you able to use a healer skill on the move should be strongly considered. Any ability that can be used on the move is intrinsically better than the ones that can't. Often you do not have the luxury of waiting to heal, if a damage dealer is forced to stop damaging, that is a bummer, his numbers are not going to be that good for the fight but unless there is an enrage in the fight it hardly will cause the fail of the ops, as **a healer delaying the heal may mean a player dies** and that can lead to a wipe quite easily.

Further, **instant abilities are better than casting time ones, even if the casting time is the same as the global cooldown**, the instant ability can be cast on the move, it cannot be interrupted or pushed back and it is front loaded(it happens before the cooldown).

For this reason the 4 pieces set bonus from Tech Medic is much better than what people give it credit for, people die during fights sometimes despite of your best efforts, worst case scenario they died out of your range, without the set you are going to have to move around trying to find your range, position yourself somewhere safe where you can cast uninterrupted, then be static for 1.5 seconds, if you take damage this can take longer due to pushback, if you are forced to move by yourself or by an effect like knockback or you are stunned, paralyzed, etc the cast gets canceled and you will have to start again, this process can take a while and make you waste precious time that you could be using to save other people, usually if someone dies it means the fight is in its roughest moments so it is a guarantee that this is the time you need to be at your best. With the set you can just run around searching the body and mash the button as soon as it lights up and proceed healing on the move avoiding hazards.

# Technical

Now that we know some of the whys, lets look at the hows.

## Gear

I am only going to talk about 2 sets because they are the ones that are both useful and accessible to most people. Concentrated Fire and Tech Medical. To fully benefit from this guide, you will need to use Tech Medical coupled with SC-4 Treatment Scan, the TLDR reason for TM over CF is the previous is aligned with the philosophy laid out in the previous section and CF goes against it.

TM makes the Kolto Shot into your best heal(more of that later) which makes it really easy to manage your resources and keep your options open, the AED set bonus help to keep you mobile. **CF forces you to adopt a heal style that focus on single target healing** and makes you spend regularly the supercharge which should be regarded as your last line defense against spike heavy damage and thus **frequently leave you powerless to respond to emergencies.** On top of that, the extra healing you get from the crits do not make up for the extra heal you get on the kolto shot and the 5s cooldown with a proc chance of 10% on heals tick on the charge generation makes it so that you at best is going to generate one or 2 extra charges per cycle of supercharge generation.

Progressive scan is one of your core heals, making it mobile and to generate resources is fantastic(from the utility thrill of the hunt) and the SC-4 tactical makes it generate up to 4 supercharges on cast.

## Laying Down the Groove

### The Groove [Kolto Shot + Progressive Scan + Kolto Missile]
Advised: Utility Thrill of the Hunt, Tactical SC-4 

This is your most basic technique and it **should be the one you master first**, you should be able to do it on your sleep, this is your default mode of healing unless the situation asks for a different approach. The skills to be used are:


- **Kolto Shot** This is your most valuable skill, if you fall asleep during the operation, you should land with your nose on the KS shortcut. By itself it heals for 3k-4k[^1], add the heal overtime from TM 7k and you get your healing range up to 10k-11k which is the same heal as the rapid scan , but the main reason that makes it the very foundation of the heal is not only it is **instant**(with all the advantages) with **no cooldown** as it actually **generates both resources**. When I first started playing the class I though this was a crappy heal that I would barely use, after some experience and the right perks and bonuses this has become the centerpiece of my gameplay.

- **Progressive Scan** This is a very strong heal, it heals the main target for 23k and the other targets for 17.4k, 11.6k and 5.8 for a total of 57.8k heal, it is a **smart healing** that will apply the secondary heals to those that need it and with the can be **cast while moving** with the utility **thrill of the hunt** and **SC-4** tactical **generates 4 charges**.

- **Kolto Missile** It is an area heal that affects up to 8 targets for 5k-7k plus another 3k over 3s for a total of up to 64k-100k, this is an **instant heal** with all the benefits of being instant.

You apply this heal in a priority list basis:

1. **Progressive Scan** is the most valuable and should be cast on whoever has less health or you deem more important to keep alive at the moment

2. **Kolto missile** There are three ways to approach this one: 
	* on lazy or panic mode: double click it to cast centered on your current target, it is not ideal but gets the job done and free you some time from looking for placement;
	* **(This should be your default)** Place it on the highest concentration of people, it works, it is likely to be effective with some luck;
	* Place on a concentration of people that know are taking damage: this is the best but requires you to have good awareness of what is going on.

3. **Kolto shot** It is your filler, anytime the other two are on cooldown keep distributing KSs around, keep in mind the HoT is on a 18s time span, so you should try to cast it on different people to maximize its benefits. In case you need to recover resources, heat or supercharge, you may choose to squeeze an extra KS before casting them.

If you do this right you should be able to do it indefinitely, your heat should be at 0 or very near 0 all times and you should be able to generate supercharge and keep it up across the entire fight. I recommend you to practice this, you don't even need a practice dummy for that, you can do it anywhere, even waiting for other players or waiting for the fight to begin.

To give you an idea what the cast would look like: PS, KM, 3xKS, KM, 2xKS then the sequence repeats PS, KM, 3xKS, KM, 2xKS. Do not get too attached to this numbers, this is what will look like when you practice but it will different if you are running around, targeting, etc. 

### The Fills [Emergency Scan + Healing Scan]

Once you get down the groove healing, if you are judicious on who you are applying the progressive scan and make sure there is kolto shots going on those that are taking damage or are likely to take damage the overall damage will be usually under control.

Still there is going to be damage spikes that surpass your groove healing and you are going to have to single target heal those that are being damaged if that is not sufficient, for that you have the combination [Emergency scan + healing scan]:

- **Emergency Scan** It is an **instant heal** for 11.6k to 13.6k with another 10k over 9 seconds for a total of 21.6k-23.6k. Besides that it applies a **10% healing bonus** to the target for 45s and it makes the **healing scan instant**, on top of all that emergency scan is **heat free**.

- **Healing Scan** It is a ~2s cast heal for 18k-20k heal, this heal should almost **never** be cast on its own, it should always be cast as an instant from emergency scan and/or when the supercharged gas is active.

Ideally you want to apply this to the tanks and try to keep the dps alive with only the groove healing and other techniques, the reason for that is that they have a moderate cooldown and the healing bonus has more value on someone you are sure will be taking damage constantly.

Here the decision process starts to get important, you have to access if the dps is taking damage as a punctual event or is going to continue to take damage, if the first is true you need to decide if healing from other effects suffices and is fast enough to bring back to ful, otherwise you need to use the combo, keep in mind that by using this on non tank toons healing is going to become harder for a while, you are going to be vulnerable to damage spikes on the tank while those abilities are on cooldown.

### Keeping the Tempo Steady [Kolto Shell + Kolto Shots]


Damage taken is not a linear function, it is going to fluctuate, sometimes it is going to get heavier, sometimes it is going to get light or even completely stop. When that happens you must seize the opportunity and recover your resources and create a buffer for the harder times. The way you do that is combining [kolto shot + kolto shell]. Here we are storing heals and resources to smooth out the healing when things get hectic.    

- **Kolto Shell** Create a protective shell that heals for 3.5k every time the target takes damage up to 7 times with a cooldown between procs of 3 seconds. That is a total of 24.5k heal that is only applied when there is damage and stays on the target for up to 3 minutes.

If you apply 6 shells and one shot you are going to net 0 heat, so you can keep applying this combination indefinitely. 

The first obvious moment you should use this is before the fight starts, by doing that you are going to build up your supercharges and have a headroom of heal going into the fight, you can keep on applying and reapplying this for as long as it takes for the group to initiate the fight so that all the shells are at theirs maximum duration and your supercharge buff is always refreshed.

During the fight you are going to need to know the fight to be able to decide if there is room to do some heal storing and which is more important to spread kolto shells to everyone or to clear the damage with kolto shells and recover supercharges if they were spent. Of course this should only be done if the group is at full or almost at full health.

Based on that you will either just throw 6 shells for every shot, for example, or one shot every shell on those that still have damage until the damage is clear and/or you have your supercharges back, maybe throw in a progressive scan for faster heal and charge buildup.

## Special tools
The previous section laid out the fundamental techniques you should practice and get familiar before you move to this, if you get them down right you are going to already be able to handle well story mode operations most of the time, you should be comfortable to use them without thinking before proceeding to try to add this to your tool set. 

### Shredding a Solo [Supercharged Gas + Healing Scan]
From time to time things are going to become real dire, you are going to have everything above on cooldown and someone is using a truck as a club to hit the tank, it becomes clear that if you do not do some massive focused healing somebody is going to die. This is what you were saving your supercharged gas for. It is time to pop it and smash the Healing Scan until it goes on cooldown.

This is not tricky to pull, the main concern with this combo is when to use it. You really should not rely on this as your main heal, this should be seem as a operation wide defensive cooldown, if you are not required to use it on a fight, that is a win. 

If emergency scan or healing scan are off cooldown, you should pop them before the supercharged gas, emergency scan first for the instant cast on the healing scan and the heal bonus, since the supercharged gas resets the cooldown for the healing scan when it is activated.

### Adding 7ths [Rapid Scan]
- **Rapid Scan** Heal for 9.6k to 11.6k and **generates 2 supercharge** stack with **no cooldown**

The reason Rapid scan is down here is that its usage is very situational and if abused it can disrupt your energy management, but when used correctly it really adds color to the healing. It is to be incorporated as part of the groove after you have become familiar with its basic usage. Rapid scan is very heat hungry it is almost the same heat as the healing scan for half as much healing and it forces you to be static, compared with kolto shot it heals for the same amount only that the whole happens at once, it also generates 2 charges of supercharge per cast.

So, purely for the heal value it gives no advantage over the kolto shot, the drive to use it could be:

1. you are pulling the target from close to death and so you want the heal up front instead of the kolto's slow build;

2. you already threw a kolto shot on the target, a second one would only override the hot effect, so if you want to throw an extra bit of healing on the same target instead of spreading kolto shots you can use the RS;

3. you just used the supercharged gas and feels you need quickly recharge the gas, you can accelerate by using rapid scan instead of some of your koltos;

4. finally, you have everything that has cooldown on cooldown and you still need balls to the wall single healing to keep someone alive.

Used on your groove rotation instead of one kolto shot for every rotation you are still going to have an stable heat, if you throw in anymore than one the heat will build up proportional to how many you are replacing. Used as a desperate measure to keep someone alive until you get other cooldowns will have you quickly build up heat, it is almost a guarantee you are going to ahve to pop your Ven Heat by the end. You can intersperse some kolto shots, but that is ineficient for single target.

## Footnotes
[^1]: The heal values are going to change with level cap adjustment and gear but the proportion between them for comparison purposes remain the same.
